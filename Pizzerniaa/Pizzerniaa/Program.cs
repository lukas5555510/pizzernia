﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzerniaa
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Adress> adres = new List<Adress>();

            Adress Główny = new Adress();
            Główny.street = "Jana Pawła 2";
            Główny.name = "Pizzeria im. Benita Mussoliniego" ;
            Główny.code = "29832845";
            Główny.phone = "123456789";
            adres.Add(Główny);
            Główny.GetAdress();

            List<PIZZA> Menu = new List<PIZZA>();

            PIZZA nyc = new PIZZA();
            nyc.Name = "nyc (0)";
            nyc.Price = 0f;
            nyc.Components ="";
            Menu.Add(nyc);

            PIZZA margaritta = new PIZZA();
            margaritta.Name = "margaritta (1)";
            margaritta.Price = 5f;
            margaritta.Components = "Ser" + " " + "Sos";
            Menu.Add(margaritta);

            PIZZA niedzielna = new PIZZA();
            niedzielna.Name = "niedzielna (2)";
            niedzielna.Price = 10f;
            niedzielna.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Ogórek";
            Menu.Add(niedzielna);

            PIZZA sobotnia = new PIZZA();
            sobotnia.Name = "sobotnia (3)";
            sobotnia.Price = 10f;
            sobotnia.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Pomidor";
            Menu.Add(sobotnia);

            PIZZA piątkowa = new PIZZA();
            piątkowa.Name = "piątkowa (4)";
            piątkowa.Price = 10f;
            piątkowa.Components = "Ser" + " " + "Sos" + " " + "Pieczarki" + " " + "Ogórek";
            Menu.Add(piątkowa);

            PIZZA czwartkowa = new PIZZA();
            czwartkowa.Name = "czwartkowa (5)";
            czwartkowa.Price = 10f;
            czwartkowa.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Kukurudza";
            Menu.Add(czwartkowa);

            PIZZA środowa = new PIZZA();
            środowa.Name = "środowa (6)";
            środowa.Price = 10f;
            środowa.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Ananas";
            Menu.Add(środowa);

            PIZZA wtorkowa = new PIZZA();
            wtorkowa.Name = "wtorkowa (7)";
            wtorkowa.Price = 10f;
            wtorkowa.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Salami";
            Menu.Add(wtorkowa);

            PIZZA poniedziałkowa = new PIZZA();
            poniedziałkowa.Name = "poniedziałkowa (8)";
            poniedziałkowa.Price = 10f;
            poniedziałkowa.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Pieczarki";
            Menu.Add(poniedziałkowa);

            PIZZA familijna = new PIZZA();
            familijna.Name = "familijna (9)";
            familijna.Price = 15f;
            familijna.Components = "Ser" + " " + "Sos" + " " + "Kurczak" + " " + "Pieczarki" + " " + "Pomidor" + " " + "Kukurydza";
            Menu.Add(familijna);

            Console.WriteLine("PIZZE:");

            foreach (PIZZA pizza in Menu)
            {
                pizza.GetPizza();
            }

            List<Sauce> SOS = new List<Sauce>();

            Sauce brak = new Sauce();
            brak.Name = "brak (0)";
            brak.Price = 0f;
            SOS.Add(brak);

            Sauce ketchup = new Sauce();
            ketchup.Name = "ketchup (1)";
            ketchup.Price = 1.5f;
            SOS.Add(ketchup);

            Sauce czosnkowy = new Sauce();
            czosnkowy.Name = "czosnkowy (2)";
            czosnkowy.Price = 1.5f;
            SOS.Add(czosnkowy);

            Sauce słodkopikantny = new Sauce();
            słodkopikantny.Name = "słodkopikantny (3)";
            słodkopikantny.Price = 1.5f;
            SOS.Add(słodkopikantny);

            Console.WriteLine("SOSY:");

            foreach (Sauce sos in SOS)
            {
                sos.GetSauce();
            }


            Console.WriteLine();
            Console.WriteLine("Wybierz pizze poprzez wpisanie jej numeru(1-9), następnie wybierz sos(0-3).");

            float zarobek = 0;

            

            List<int> Orders = new List<int>();
            Orders.Add(1);
            Orders.Add(2);
            Orders.Add(3);
            Orders.Add(4);
            Orders.Add(5);

            foreach (var zamówienie in Orders)
            {
                int zamówienie1 = int.Parse(Console.ReadLine());
                int sos = int.Parse(Console.ReadLine());
                
                Console.WriteLine(Menu[zamówienie1].Components);
                Console.WriteLine(SOS[sos].Name);

                zarobek = zarobek + Menu[zamówienie1].Price + SOS[sos].Price;
            }




            Console.WriteLine(zarobek+"zł");
            Console.WriteLine();




            Console.Read();
        }
    }
}
